import { Component, OnInit, Input } from '@angular/core';
import { Client } from '../client.model';

@Component({
  selector: 'app-input-binding',
  templateUrl: './input-binding.component.html',
  styleUrls: ['./input-binding.component.css']
})
export class InputBindingComponent implements OnInit {

  @Input() name: string;
  @Input() lastName: string;
  @Input() age: number;

  clients: Client[];

  constructor() {
    this.clients =   [
      {
        id: 1,
        name: 'Andreson',
        age: 34,
      },
      {
        id: 2,
        name: 'Loide',
        age: 50,
      },
      {
        id: 3,
        name: 'Vagdon',
        age: 60,
      },
      {
        id: 4,
        name: 'Adriele',
        age: 28,
      },
    ];
  }

  ngOnInit(): void {
  }

}
