import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ng-for-form',
  templateUrl: './ng-for-form.component.html',
  styleUrls: ['./ng-for-form.component.css']
})
export class NgForFormComponent implements OnInit {

  name = '';
  address = '';
  phone = '';
  city = '';
  age = 0;

  cities = [
    { name: 'Paudalho', state: 'PE' },
    { name: 'Carpina', state: 'PE' },
    { name: 'Lagoa do Carro', state: 'PE' },
    { name: 'Limoeiro', state: 'PE' }
  ];

  clientes = [];

  constructor() { }

  ngOnInit() {
  }

  save() {
    this.clientes.push(
      {
        name: this.name,
        address: this.address,
        phone: this.phone,
        city: this.city,
        age: this.age
      }
    );
    console.log(this.clientes);
  }

  cancel() {
    this.name = '';
    this.address = '';
    this.phone = '';
    this.city = '';
    this.age = 0;
  }

  delete(i: number) {
    this.clientes.splice(i, 1);
  }
}
