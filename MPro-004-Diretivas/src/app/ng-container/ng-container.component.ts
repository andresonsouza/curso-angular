import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ng-container',
  templateUrl: './ng-container.component.html',
  styleUrls: ['./ng-container.component.css']
})
export class NgContainerComponent implements OnInit {

  users = [
    { login: 'Jesus', role: 'admin', lastlogin: new Date('2/30/2020') },
    { login: 'Vagon', role: 'user', lastlogin: new Date('4/25/2020') },
    { login: 'Loide', role: 'user', lastlogin: new Date('3/27/2020') },
    { login: 'Andreson', role: 'user', lastlogin: new Date('5/17/2020') },
    { login: 'Adriele', role: 'user', lastlogin: new Date('5/27/2020') },
    { login: 'Acsa', role: 'user', lastlogin: new Date('5/7/2020') }
  ];

  constructor() { }

  ngOnInit() {
  }

}
