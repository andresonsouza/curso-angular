export interface Book {
  title: string;
  pages: number;
  autors: string[];
}
